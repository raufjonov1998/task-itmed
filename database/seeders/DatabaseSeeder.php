<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        Author::factory(50)->create();
        Book::factory(100)->create();

        $book_types = ['Action and Adventure', 'Classics', 'Science fiction',
        'Detective and Mystery', 'Fantasy', 'Historical Fiction', 'Horror', 'Women’s fiction'];

        if (\Illuminate\Support\Facades\Schema::hasTable('book_types')) {
            foreach ($book_types as $type_name) {
                DB::table('book_types')->insert(['name' => $type_name]);
            }
        }
    }
}
