<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AuthorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'surname' => $this->faker->lastName,
            'name' => $this->faker->firstName,
            'birthday' => $this->faker->dateTimeBetween('1900-01-01', '2000-12-31'),
            'description' => $this->faker->text(100)
        ];
    }
}
