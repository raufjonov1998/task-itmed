<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'author_id' => rand(1, 50),
            'book_type_id' => rand(1,8),
            'name' => $this->faker->name,
            'code' => $this->faker->postcode
        ];
    }
}
