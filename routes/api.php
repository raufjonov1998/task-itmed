<?php

use App\Http\Controllers\Api\AuthorController;
use App\Http\Controllers\Api\BookController;
use App\Http\Controllers\Api\JWTAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

    Route::post('login', [JWTAuthController::class, 'login']);
    Route::post('logout', [JWTAuthController::class, 'logout']);

    Route::group(['name' => 'authors'], function(){
        Route::group(['prefix' => 'authors'], function(){});
        Route::apiResource('authors', AuthorController::class);
    });

    Route::group(['name' => 'books'], function(){
        Route::group(['prefix' => 'books'], function(){
            Route::post('downloadPDF', [BookController::class, 'downloadPDF']);
        });
        Route::apiResource('books', BookController::class);
    });
