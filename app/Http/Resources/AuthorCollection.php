<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AuthorCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public $collects = AuthorResource::class;
	
    public function toArray($request)
    {
        return [
            'authors' => $this->collection,
            'pagination' => [
                'total'        => $this->total(),
                'count'        => $this->count(),
				'per_page'     => $this->perPage(),
				'current_page' => $this->currentPage(),
            ]
        ];
    }
}
