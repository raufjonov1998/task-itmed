<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequest;
use App\Models\Book;
use Illuminate\Http\Request;
use EllipseSynergie\ApiResponse\Laravel\Response;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    protected $response;
    protected $book;
    protected $per_page;

    public function __construct(Response $response, Book $book)
    {
        $this->middleware('jwt.auth');
        $this->middleware('permission')->only(['store', 'update', 'destroy']);

        $this->response = $response;
        $this->book = $book;
        $this->per_page = request('per_page') ? request('per_page') : 10000000;
    }

    public function index()
    {
        $books = $this->book;
        $books = $books->with('author:id,surname,name', 'book_type:id,name');
        $books = $books->orderBy('created_at', 'DESC')->paginate($this->per_page);

        return $this->response->withArray([
            'result' => [
                'data' => [
                    'books' => $books->items(),
                    'pagination' => [
                        'currentPage' => $books->currentPage(),
                        'total' => $books->total()
                    ]
                ]
            ]
        ]);
    }

    public function store(BookRequest $request)
    {
        Book::create($request->all());

        return $this->response->withArray([
            'result' => [
                'success' => true,
				'message' => "Book created successfully!",
				'data' => []
            ]
        ]);
    }

    public function show(Book $book)
    {
        return $this->response->withArray([
            'result' => [
                'success' => true,
				'data' => [
                    'book' => new \App\Http\Resources\BookResource($book)
                ]
            ]
        ]);
    }

    public function update(BookRequest $request, Book $book)
    {
        $book->update($request->all());

        return $this->response->withArray([
            'result' => [
                'success' => true,
				'message' => "Book updated successfully!",
				'data' => []
            ]
        ]);
    }

    public function destroy(Book $book)
    {
        $book->delete();

        return $this->response->withArray([
            'result' => [
                'success' => true,
				'message' => "Book deleted successfully!",
				'data' => []
            ]
        ]);
    }

    public function downloadPDF(){
        $file_name = request('file_name');
        $file_path = '/public/books/'. $file_name;
		
        if ($file_name && Storage::exists($file_path)) {
            return Storage::download($file_path);
        }

        return response()->json([
            'result' => [
                'success' => false
            ],
            'error' => [
                'code' => 404,
                'message' => 'File not found'
            ]
        ])->setStatusCode(404);
    }
}
