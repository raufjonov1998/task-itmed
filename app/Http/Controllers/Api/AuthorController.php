<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthorRequest;
use App\Models\Author;
use EllipseSynergie\ApiResponse\Laravel\Response;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    protected $response;
    protected $author;
    protected $per_page;

    public function __construct(Response $response, Author $author)
    {
        $this->middleware('jwt.auth');
        $this->middleware('permission')->only(['store', 'update', 'destroy']);

        $this->response = $response;
        $this->author = $author;
        $this->per_page = request('per_page') ? request('per_page') : 10000000;
    }
	
    public function index()
    {
        $authors = $this->author;
        $authors = $authors->select('*')->orderBy('created_at', 'DESC');
        $authors = $authors->paginate($this->per_page);

        return $this->response->withArray([
            'result' => [
                'data' => new \App\Http\Resources\AuthorCollection($authors)
            ]
        ]);
    }

    public function store(AuthorRequest $request)
    {
        Author::create($request->all());

        return $this->response->withArray([
            'result' => [
                'success' => true,
				'message' => "Author created successfully!",
				'data' => []
            ]
        ]);
    }

    public function show(Author $author)
    {
        return $this->response->withArray([
            'result' => [
                'success' => true,
				'data' => [
                    'book' => new \App\Http\Resources\AuthorResource($author)
                ]
            ]
        ]);
    }

    public function update(AuthorRequest $request, Author $author)
    {
        $author->update($request->all());

        return $this->response->withArray([
            'result' => [
                'success' => true,
				'message' => "Author updated successfully!",
				'data' => []
            ]
        ]);
    }

    public function destroy(Author $author)
    {
        $author->delete();

        return $this->response->withArray([
            'result' => [
                'success' => true,
				'message' => "Author deleted successfully!",
				'data' => []
            ]
        ]);
    }
}
