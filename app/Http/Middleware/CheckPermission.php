<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();
        if ($user->role) {
            return $next($request);
        }

        return response()->json([
            'result' => [
                'success' => false
            ],
            'error' => [
                'code' => 403,
                'message' => __('messages.permission_denied')
            ],
            'message' => __('messages.permission_denied')
        ]);
        //abort(403, __('messages.permission_denied'));
    }
}
