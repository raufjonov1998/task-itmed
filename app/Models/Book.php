<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    const TABLE_NAME = 'books';

    protected $fillable = ['author_id', 'book_type_id', 'name', 'code'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i',
        'updated_at' => 'datetime:Y-m-d H:i',
    ];

    public function author(){
        return $this->belongsTo(Author::class);
    }
	
    public function book_type(){
        return $this->belongsTo(BookType::class);
    }
}
